syntax on               " Switch syntax highlighting on, when the terminal has colors

set nobackup            " No backup files
set nowritebackup       " No write backup
set noswapfile          " No swap file
set history=100         " Command history
set nowrap              " Turn word wrap off
set number              " Turn on line numbers
set wildmenu            " Visual autocomplete for command menu (e.g. :e ~/path/to/file)
set showmatch           " highlight a matching [{()}] when cursor is placed on start/end character
set incsearch           " Incremental searching (search as you type)
set hlsearch            " Highlight search matches
set ignorecase          " Ignore case in search
set smartcase           " Make sure any searches /searchPhrase doesn't need the \c escape character
set showcmd             " Show incomplete commands
set hidden              " Required for operations modifying multiple buffers like rename.

set expandtab           " Convert tabs to spaces
set tabstop=4           " Set tab size in spaces (this is for manual indenting)
set shiftwidth=4        " The number of spaces inserted for a tab (used for auto indenting)

" Better splits (new windows appear below and to the right)
set splitbelow
set splitright

set list listchars=tab:\ \ ,trail:· " Highlight tailing whitespace
set signcolumn=yes
set colorcolumn=80


noremap zy "+y          " Yank to X clipboard


" Vundle plugin management
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'chriskempson/vim-tomorrow-theme'

Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-dispatch'
Plugin 'junegunn/vim-easy-align'
Plugin 'dhruvasagar/vim-table-mode'

" Plugin 'rust-lang/rust.vim'
Plugin 'zrkn/rust.vim'
Plugin 'racer-rust/vim-racer'

" Plugin 'prabirshrestha/async.vim'
" Plugin 'prabirshrestha/vim-lsp'

Plugin 'sheerun/vim-polyglot'
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'christoomey/vim-tmux-navigator'

call vundle#end()
filetype plugin indent on


colorscheme Tomorrow-Night

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" if executable('rls')
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'rls',
"         \ 'cmd': {server_info->['rustup', 'run', 'nightly', 'rls']},
"         \ 'whitelist': ['rust'],
"         \ })
" endif
"
" nmap gd :LspDefinition<CR>
" nmap gc :LspDocumentDiagnostics<CR>

au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap gc :tab split<CR>:call racer#GoToDefinition()<CR>
au FileType rust nmap <leader>gd <Plug>(rust-doc)

fun! StripTrailingWhitespace()
  " don't strip on these filetypes
  if &ft =~ 'markdown'
    return
  endif
  %s/\s\+$//e
endfun
autocmd BufWritePre * call StripTrailingWhitespace()

nmap mtt :Make test<CR>
nmap mt<Space> :Make test<Space>

nmap mcc :Make check<CR>
nmap mc<Space> :Make check<Space>

let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
